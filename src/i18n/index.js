import enUS from './en-us'
import cat from './ca-es'
export default {
  'en-us': enUS,
  'ca-es': cat
}
