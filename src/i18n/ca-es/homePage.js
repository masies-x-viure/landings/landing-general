export default {
  intro: {
    title: 'L\'APP PER CREAR LA TEVA COOPERATIVA D\'HABITATGE',
    subtitle: 'Per un retorn al món rural conscient i respectuós amb l\'entorn i els seus habitants',
    subtitle2: 'Impulsem junts la reactivació social i el retorn de la vida al camp'
  },
  features: {
    title: 'TOT EL QUE NECESSITES',
    subTitle: '',
    list: [
      {
        icon: "las la-search",
        title: "Troba gent",
        text: "Descobreix persones que tinguin un projecte que sigui compatible o fins i tot complementari al teu."
      },
      {
        icon: "las la-comments",
        title: "Xateja",
        text: "MxV ofereix una xat, que permet conectar amb altres persones o grups. Crea nous vincles i troba les persones amb qui conviure."
      },
      {
        icon: "las la-users",
        title: "Crea el teu grup",
        text: "Reuneix les persones afins al teu projecte i gestiona la cooperativa de manera conjunta."
      },
      {
        icon: "las la-cloud-upload-alt",
        title: "Comparteix documents",
        text: "MxV facilita l’ús compartit de recursos digitals i documents rellevants per la creació i gestió de la cooperativa"
      },
      {
        icon: "las la-pencil-ruler",
        title: "Cerca recursos",
        text: "Per crear la cooperativa MxV afavoreix l’autonomia del grup i connecta el grup amb tècnics especialitzats."
      },
      {
        icon: "las la-cog",
        title: "Gestiona la cooperativa",
        text: "Administra els recursos i tasques de la creació i gestió de la teva cooperativa de manera intuitiva."
      }
    ]
  },
  resources: {
    title: 'RECURSOS PER A LA FORMACIÓ DE LA COOPERATIVA',
    subtitle: 'Amb MxV es facilita la creació i l’autonomia del grup i s\'el connecta amb tècnics especialitzats',
    list: [
      {
        img: '/img/resources/people/priscilla-du-preez-W3SEyZODn8U-unsplash.jpg',
        title: "Grups de persones",
        text: "Descobreix gent que busqui el mateix que tu, que comparteixi la teva idea de vida i que complementi el teu projecte de futur.",
        link: "/info#grup-de-persones"
      },
      {
        img: '/img/resources/houses/vall-major.jpeg',
        title: "Habitatge rural",
        text: "Troba l'espai perfecte per emprendre el projecte de viure al món rural de forma cooperativa.",
        link: "/info#habitatge-rural"
      },
      {
        img: '/img/resources/assessorament/bantersnaps-PTRzqc_h1r4-unsplash.jpg',
        title: "Assessorament",
        text: "Rep assessorament legal, arquitectònic i tècnic per facilitar el procés de crear una cooperativa d'habitatge.",
        link: "/info#assessorament"
      },
      {
        img: '/img/resources/technicians/spiral-roof-masies_x_viure.jpg',
        title: "Tècnics i sostenibilitat",
        text: "Tots els recursos necessaris per construir el teu projecte. Troba bioconstructors, arquitectes, instal·ladors d'energia renovable, materials sostenibles, etc.",
        link: "/info#tècnics-i-sostenibilitat"
      },
      {
        img: '/img/resources/financament/micheile-henderson-ZVprbBmT8QA-unsplash.jpg',
        title: "Finançament",
        text: "Com finançar el teu projecte de manera ètica y responsable? Hi ha opcions més enllà de la banca tradicional.",
        link: "/info#finançament"
      }
    ]
  },
  howItWorks: {
    title: 'COM FUNCIONA?',
    img: '',
    btnText: '+ Informació',
    list: [
      'Cerca persones i habitatges per necessitats, zones i interessos. I connecta amb altres per a crear el grup de la cooperativa!',
      'Aconsegueix assessorament per la constitució d’uns estatuts per la cooperativa, les seves dinàmiques de grup, etc',
      'Feu-vos la casa a la vostra manera amb bioconstructors, arquitectes, intal·ladors d’energies verdes i/o dissenyadors de permacultura. Amb ells podreu alinear la casa amb la natura per a adequar-la a les vostres necessitats de manera sostenible',
      'Troba fòrmules de finançament amb capital de la banca ètica'
    ]
  },
  keepPosted: {
    title: 'MANTEN-TE INFORMAT',
    subtitle: 'Estem desenvolupant l\'App!',
    underDevelopment: 'Estem treballant en l\'APP',
    text: 'Rep informació sobre el desenvolupament de MasiesxViure, l\'aplicació per a persones i grups que volen crear una cooperativa d\' habitatge al món rural.',
    terms: '*El teu correu només serà fet servir per a informar-te de les novetats',
    btnText: 'Envia',
    img: '/img/'
  },
  footer: {
    links: {
      title: 'Masies x Viure',
      links: [
          {
            text: "+ Informació",
            link: "masies-x-viure"
          },
          {
            text: "Què és?",
            link: "masies-x-viure"
          },
          {
            text: "Cooperativisme com a llavor",
            link: "cooperativisme-com-a-llavor-pel-canvi"
          },
          {
            text: "Objectius",
            link: "objectius"
          },
          {
            text: "Com funciona",
            link: "com-funciona"
          }
        ]
    },
    resources: {
      title: 'Recursos',
      links: [
          {
            text: 'Recursos',
            link: "recursos-per-crear-la-cooperativa"
          },
          {
            text: "Grups de persones",
            link: "grup-de-persones"
          },
          {
            text: "Habitatge rural",
            link: "habitatge-rural"
          },
          {
            text: "Assessorament",
            link: "assessorament"
          },
          {
            text: "Tècnics i sostenibilitat",
            link: "tècnics-i-sostenibilitat"
          },
          {
            text: "Finançament",
            link: "finançament"
          }
        ]
    },
    about: {
      title: 'Vols Col·laborar-hi?',
      links: [
        {
          text: 'Ens calen mans i bones idees!'
        },
        {
          text: 'També tècnics en bioconstrucció,'
        },
        {
          text: 'Proveïdors de materials sostenibles'
        },
        {
          text: 'i Assessors d\'impuls d\'habitatge'
        },
        {
          text: 'Escriu-nos!'
        },
        {
          text: 'info@masiesxviure.cat'
        }
      ]
    }
  }
}
