import Vue from 'vue'
import axios from 'axios'

const instance = axios.create({
  baseURL: 'https://masies-x-viure--be-newsletter.herokuapp.com/api/',
  timeout: 1000,
  headers: {
    'Content-Type': 'application/json'
  }
})

Vue.prototype.$axios = instance
