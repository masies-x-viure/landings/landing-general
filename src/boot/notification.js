import Vue from 'vue'
import { Notify } from 'quasar'

const notification = (message, type = 'positive', position = 'top') => {
  Notify.create({
    message,
    type,
    position,
    actions: [
      { label: 'X', color: 'white' }
    ]
  })
}

Vue.prototype.$notification = notification
