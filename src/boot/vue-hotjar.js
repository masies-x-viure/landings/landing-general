import Vue from 'vue'
import Hotjar from 'vue-hotjar'

Vue.use(Hotjar, {
  isProduction: true,
  snippetVersion: 6,
  id: '2215619' // Hotjar Site ID
})
